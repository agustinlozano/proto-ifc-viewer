import viewer from './initViewer'
import { bimtrazerSortDev } from '../modules/sorting'
import { magicLogger } from '../utils/logger'

async function loadIfc (changed) {
  const file = changed.target.files[0]
  const ifcURL = URL.createObjectURL(file)
  const myModel = await viewer.IFC.loadIfcUrl(ifcURL)
  magicLogger('0. MyModel listo')

  setTimeout(async () => { bimtrazerSortDev(myModel.modelID) }, 1500)
}

export default loadIfc
