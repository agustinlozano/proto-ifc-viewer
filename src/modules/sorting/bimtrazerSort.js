import { getPropertySet, getAllBtzParams } from '../getting'
import { generateRandomId, renderFifteenJsonObjects, validateAnArray } from '../../utils'
import {
  buildBtzBlocksV4,
  filterPropertiesIds,
  formatDates,
  sortPropertiesV4
} from './sortStuff'
import { storeBlocks } from '../../services/storeBlocks'
import { checkException, validate } from '../../utils/validate'
import { magicLogger } from '../../utils/logger'

export async function bimtrazerSortDev (modelID) {
  const rawDictionary = await getAllBtzParams(modelID)
  magicLogger('rawDictionary', rawDictionary)

  formatDates(rawDictionary.endDates)

  const { descriptions: rawBtzDescriptions } = rawDictionary
  magicLogger('1. Diccionario listo')

  const result = validateAnArray(
    rawBtzDescriptions,
    'BimtrazerSort: There is no btz parameter.')

  checkException(result, 'Cannot load the IFC model because btz-parameter is missing.')

  // Obtener las propiedades de la clase PropertiesSet
  const rawPropsSet = await getPropertySet(
    filterPropertiesIds(rawBtzDescriptions),
    modelID)
  magicLogger('2. rawPropSet listo')

  validateAnArray(
    rawPropsSet,
    'BimtrazerSort: There was a problem while filtering the parameter.')

  // Pre-costruir los bloques
  const prebuiltBlocksv4 = sortPropertiesV4(rawDictionary)
  magicLogger('prebuiltBlocksV4', prebuiltBlocksv4)
  magicLogger('3. PrebuiltBlockV4 listo')

  validateAnArray(
    prebuiltBlocksv4,
    'BimtrazerSort: There was a problem while prebuilding the blocks.')

  // Terminar de construir el bloque
  const btzBlocksV4 = await buildBtzBlocksV4(rawPropsSet, prebuiltBlocksv4)
  magicLogger('4. btzBlocksV4', btzBlocksV4)

  validateAnArray(
    btzBlocksV4,
    'BimtrazerSort: There was a problem while building the blocks.')

  const projId = generateRandomId()
  const res = await storeBlocks(projId, btzBlocksV4, 'BlocksIFC')
  magicLogger('5. storeBlocks response: ', res)

  validate(res, 'BimtrazerSort: There was a problem while storing the blocks.')

  renderFifteenJsonObjects(btzBlocksV4, 'btzBlock')
}
