import { magicLogger } from '../utils/logger'

export async function getBlockCodes (numberOfBlocks) {
  const URL = `http://projects.bimtrazer.com/api/GetBlocks/${numberOfBlocks}`
  const options = {
    method: 'GET'
  }

  try {
    const res = await fetch(URL, options)
    const {
      DATA: data,
      DESCRIPCION: status,
      ID: id
    } = await res.json()

    magicLogger('getBlockCodes response', {
      DATA: data,
      DESCRIPTION: status,
      ID: id
    })

    if (status === 'Successful' && id === '00') {
      return data
    }

    return null
  } catch (error) {
    console.error('There was an error: ', error)
    return null
  }
}
